// A2
using System;
using System.Text.Json;
using System.Text.Json.Serialization;


namespace DZ
{
    public class Company
    {
        public string Title { get; set; }
    }
    public class Unit
    {
        public string Title { get; set; }
        public Company company { get; set; }
        public Unit upperunit { get; set; }
    }
    public class Worker
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Position { get; set; }
        public string Birth { get; set; }
        public bool IsChief { get; set; }
     
    }
    class Program
    {
        static void Main(string[] args)
        {
            Company UM = new Company
            {
                Title = "Umbrella",

            };
            Unit Zomb = new Unit
            {
                Title = "Zombie",
                company = UM,
             
            };
            Unit QZomb = new Unit
            {
                Title = "Cute Zombie",
                company = UM,
                upperunit = Zomb,

            };
            Worker Alan = new Worker
            {
                Name = "Alan",
                Surname = "Wake",
                Position = "Test subject",
                Birth = "19/12/2222",
                IsChief = false,
            };
            Worker Lebo = new Worker
            {
                Name = "Big",
                Surname = "Lebowski",
                Position = "Just being cool guy",
                Birth = "18/11/2222",
                IsChief = true,
            };


            string json = JsonSerializer.Serialize<Worker>(Lebo);
            Console.WriteLine(json);
            Console.ReadKey();
        }
    }
}
